package pl.akai.bookcrossing.list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.akai.bookcrossing.login.CurrentUserService;
import pl.akai.bookcrossing.model.Book;
import pl.akai.bookcrossing.model.User;

import java.util.List;

@Component
public class BookBean {

    private final BookDao bookDao;
    private final CurrentUserService currentUserService;


    @Autowired
    public BookBean(BookDao bookDao, CurrentUserService currentUserService) {
        this.currentUserService = currentUserService;
        this.bookDao = bookDao;
    }

    public void insertBook(Book book) {
        User user = currentUserService.getCurrentUser();
        book.setOwner(user);
        bookDao.insertBook(book);
    }

    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    public Book getBookById(Integer id) {
        return bookDao.getBookById(id);
    }

    public int getLastInsertedBookId() {
        User user = currentUserService.getCurrentUser();
        return bookDao.getInsertedBookIdByUserId(user.getId());
    }
}
